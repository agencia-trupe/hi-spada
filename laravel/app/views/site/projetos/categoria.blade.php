@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-projetos conteudo-categoria">
        <div class="interna">
            <h1>Projetos <span class="separador">|</span> {{ strtolower( $categoria->titulo )}}</h1>
            <div class="separador-titulo"></div>
            <div class="separador-mais"></div>
            <div class="clearfix"></div>
            <div class="projetos-links">
                @foreach ($categoria->projetos as $projeto)
                    <a href="{{ URL::route('projeto', array($categoria->slug, $projeto->slug)) }}" class="projeto-link">
                        <img src="{{ URL::asset(Image::thumb($projeto->imagem, 170, 170)) }}" alt="Hispada - Projetos - {{ $categoria->titulo}} - {{ $projeto->titulo }}">
                        <span class="image-hover"></span>
                    </a>
                @endforeach
                    <div class="clearfix"></div>
            </div>
            <div class="interna"></div>
        </div>
    </div>
@stop
