<?php namespace App\Controllers\Site;

use View;

class ProfissionaisController extends \BaseController
{
    public function index()
    {
        return View::make('site.profissionais.index');
    }
}
