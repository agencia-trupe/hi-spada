<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Categoria, Image, Midia;

class MidiasController extends BaseController
{
    /**
     * Categoria Repository
     *
     * @var Categoria
     */
    protected $midia;

    public function __construct(Midia $midia)
    {
        $this->midia = $midia;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $midias = $this->midia->ordered()->get();

        return View::make('painel.midias.index', compact('midias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //Array com categorias disponíveis e seus respectivos ids como chave.
        $categorias = array('' => 'Selecione uma categoria') + Categoria::lists('titulo', 'id');

        return View::make('painel.midias.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), array('_method', 'midia_id'));;
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Midia::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'midias/destaque');
            }

            $midia = new Midia($input);
            $midia->save();

            return Redirect::route('painel.midias.edit', $midia->id);
        }

        return Redirect::route('painel.midias.create')
            ->withInput()
            ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $midia = $this->midia->findOrFail($id);

        return View::make('painel.midias.show', compact('midia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $midia = $this->midia->find($id);

        if (is_null($midia)) {
            return Redirect::route('painel.midias.index');
        }

        return View::make('painel.midias.edit', compact('midia', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem', 'object_id'));
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Midia::$rules);

        if ($validation->passes()) {
            $midia = $this->midia->find($id);

             if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'midias/destaque');
                @unlink(public_path(substr($projeto->imagem, 1)));
            }

            $midia->update($input);

            return Redirect::route('painel.midias.show', $id);
        }

        return Redirect::route('painel.midias.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $midia = $this->midia->find($id);

        @unlink(public_path(substr($midia->imagem, 1)));

        $midia->delete();

        return Redirect::route('painel.midias.index');
    }

}
