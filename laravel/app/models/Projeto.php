<?php

class Projeto extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' 	=> 	'required|max:255',
        'slug'	 	=> 	'required|max:255',
        'categoria_id' => 'required|integer',
    );

    public function categoria()
    {
        return $this->belongsTo('Categoria');
    }

    public function projetoFotos()
    {
        return $this->hasMany('Projetofoto')->ordered();
    }
}
