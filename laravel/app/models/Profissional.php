<?php

class Profissional extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'nome' => 'required',
        'telefone' => 'required',
        'email'		=>	'required|email',
        'endereco' => 'required',
    );
}
