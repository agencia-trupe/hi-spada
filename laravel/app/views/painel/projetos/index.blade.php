@extends('painel._layouts.default')

@section('main')

<h1>Projetos</h1>

<p>{{ link_to_route('painel.projetos.create', 'Novo projeto', null, array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="projeto" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($projetos->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody class="sortable">
            @foreach ($projetos as $projeto)
                <tr id="projeto_{{$projeto->id}}">
                    <td>{{ $projeto->categoria->titulo }}</td>
                    <td>{{{ $projeto->titulo }}}</td>
                    <td>{{{ $projeto->slug }}}</td>
                    <td>
                        @if ( strlen($projeto->descricao) > 100) {{ substr($projeto->descricao, 0, 100) }}[...]
                        @else {{ $projeto->descricao }}
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($projeto->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.projetos.edit', 'Editar', array($projeto->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.projetos.destroy', $projeto->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="lead">
        Nenhum projeto cadastrado.
    </p>
@endif

@stop
