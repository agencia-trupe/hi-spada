@extends('painel._layouts.default')

@section('main')

<h1>Profissionais</h1>

{{ link_to_route('painel.profissionais.create', 'Novo profissional', null, array('class' => 'btn btn-mini btn-info')) }}</p>

@if ($profissionals->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($profissionals as $profissional)
                <tr>
                    <td>{{{ $profissional->nome }}}</td>
                    <td>{{{ $profissional->email }}}</td>
                    <td>{{ $profissional->telefone }}</td>
                    <td>{{ link_to_route('painel.profissionais.edit', 'Editar', array($profissional->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.profissionais.destroy', $profissional->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="lead">
        Nenhum profissional cadastrado.
    </p>
@endif

@stop
