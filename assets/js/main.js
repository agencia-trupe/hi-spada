//Placeholders para todos os inputs
$('input[placeholder], textarea[placeholder]').placeholderEnhanced();

//Slideshow Home
$('.home-slider').bxSlider({
	controls: false,
	auto: true
});

//Envio do formulário de contato
$('#form-contato').submit(function(e){
	e.preventDefault();
	var post = {
		nome: $('#form-contato [name="nome"]').val(),
		email: $('#form-contato [name="email"]').val(),
		mensagem: $('#form-contato [name="mensagem"]').val()
	}
	$.ajax({
		url: 'contato',
		type: 'POST',
		dataType: 'json',
		data: post,
	})
	.done(function(data) {
		if(data.errors)
		{
			alert(data.msg + "\n" + data.errors);
		} else {
			$('input[type="text"]').each(function(index, el) {
				$(this).val('');
			});

			alert(data.msg);
		}
	});
});

//Tabs projetos
$('.projeto-tabs-main-slider').bxSlider({
	pagerCustom: '.projeto-tabs-index',
});

//Index de Tabs Projetos
var tabsIndexSlider = $('.projeto-tabs-index').bxSlider({
	pager: false,
	infiniteLoop: false,
	hideControlOnEnd: true,
	onSliderLoad: function(){
		if(tabsIndexSlider.getSlideCount() <= 1)
		{
			$('.bx-controls').hide();
		}
	}
});

$(window).load(function(){
	$(".gray").animate({opacity:1},500);

	// clone image
	$('.gray').each(function(){
		var el = $(this);
		el.css({"position":"absolute"}).wrap("<span class='img_wrapper' style='display: inline-block'>").clone().addClass('img_grayscale').css({"position":"absolute","z-index":"998","opacity":"1"}).insertBefore(el).queue(function(){
			var el = $(this);
			el.parent().css({"width":this.width,"height":this.height});
			el.dequeue();
		});

		if($.browser.msie){
			this.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(grayScale=1)';
		} else {
			this.src = grayscale(this.src);
		}
	});

	// Fade image
	$('.gray').mouseover(function(){
		$(this).parent().find('img:first').stop().animate({opacity:0}, 500);
	})
	$('.img_grayscale').mouseout(function(){
		$(this).stop().animate({opacity:1}, 500);
	});
});

function grayscale(src){
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	var imgObj = new Image();
	imgObj.src = src;
	canvas.width = imgObj.width;
	canvas.height = imgObj.height;
	ctx.drawImage(imgObj, 0, 0);
	var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
	for(var y = 0; y < imgPixels.height; y++){
		for(var x = 0; x < imgPixels.width; x++){
			var i = (y * 4) * imgPixels.width + x * 4;
			var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
			imgPixels.data[i] = avg;
			imgPixels.data[i + 1] = avg;
			imgPixels.data[i + 2] = avg;
		}
	}
	ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	return canvas.toDataURL();
}

$(document).ready(function() {
	var publiSlides = $('.publicacoes-lightbox-slider.multiple').bxSlider({
		pager: false,
		infiniteLoop: false,
		hideControlOnEnd: true,
	});

	var publiSlidesSingle = $('.publicacoes-lightbox-slider.single').bxSlider({
		pager: false,
		infiniteLoop: false,
		controls: false,
	});
});

var nLightBoxFade = $('.nlightbox-overlay');

$('[role="lightbox"]').click(function(){
	$('[data-lightbox-id="' + $(this).data('shadow-id') + '"]' ).css({'opacity' : '1', 'z-index':'1002'});
	nLightBoxFade.show();
	$('body, html').animate({
		'scrollTop' : 150
	}, 250);
	return false;
})
$('.nlightbox-close').click(function(event) {
	$(this).parent().css({'opacity':'0', 'z-index':'0'});
	nLightBoxFade.hide();
	return false;
});
nLightBoxFade.click(function(){
	$(this).hide();
	$('.nlightbox-content-wrapper').css({'opacity':'0','z-index':'0'});
});

//Submit do formulário de profissionais
$('#profissionais-form').submit(function(event){
	event.preventDefault();
	$.ajax({
		url: 'profissionais',
		type: 'POST',
		dataType: 'json',
		data: $('#profissionais-form').serialize(),
	})
	.done(function(data) {
		if(data.errors){
			var output = '';
			for (property in data.errors) {
			  output += data.errors[property]+"\n";
			}
			alert(output);
		} else {
			alert(data.message);
		}
	})
});

//Label do campo de mensagens do formulário de contato
$('.legenda.mensagem').click(function(){
	$('#mensagem').focus();
})

/**
* Upload de Crrículos
*/
$('#form-trabalhe').submit(function(e) {

	e.preventDefault();
	$.ajaxFileUpload({
	    url         	: $(this).data('send-url'),
	    secureuri      	: false,
	    fileElementId  	: 'curriculo',
	    dataType    	: "JSON",
	    accepts: {
		  	json: 'application/json'
		},
	    data        : {
	        'nome': $('#form-trabalhe [name="nome"]').val(),
	        'email': $('#form-trabalhe [name="email"]').val()
	    },
	    success  : function (data)
	    {
	        data = JSON.parse(data);
	        if(data.errors)
	        {
	        	alert(data.errors);
	        } else {
	        	alert(data.msg);
	        }
	    }
	});
	return false;
});
