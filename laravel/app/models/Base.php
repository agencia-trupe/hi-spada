<?php

class Base extends \Eloquent
{
    public function scopeOrdered($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
}
