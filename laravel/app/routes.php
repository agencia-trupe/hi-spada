<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Model Repositories Binding
 */
App::bind('ProjetoFoto\ProjetoFotoRepositoryInterface', 'ProjetoFoto\ProjetoFoto');

/**
 * Rotas de autenticação
 */
Route::get('painel/logout',  array('as' => 'painel.logout',      'uses' => 'App\Controllers\Painel\AuthController@getLogout'));
Route::get('painel/login',   array('as' => 'painel.login',       'uses' => 'App\Controllers\Painel\AuthController@getLogin'));
Route::post('painel/login',  array('as' => 'painel.login.post',  'uses' => 'App\Controllers\Painel\AuthController@postLogin'));
Route::resource('groups', 'App\Controllers\Painel\GroupController');


/**
 * Rotas do painel
 */
Route::group(array('prefix' => 'painel', 'before' => 'auth.admin'), function() {
    Route::any('/', 'App\Controllers\Painel\PaginasController@index');
    Route::resource('paginas', 'App\Controllers\Painel\PaginasController');
    Route::resource('categorias', 'App\Controllers\Painel\CategoriasController');
    Route::resource('projetos', 'App\Controllers\Painel\ProjetosController');
    Route::resource('midias', 'App\Controllers\Painel\MidiasController');
    Route::resource('users', 'App\Controllers\Painel\UsersController');
    Route::resource('socials', 'App\Controllers\Painel\SocialsController');
    Route::resource('slides', 'App\Controllers\Painel\SlidesController');
    Route::resource('locais', 'App\Controllers\Painel\LocalsController');
    Route::resource('profissionais', 'App\Controllers\Painel\ProfissionalsController');

    //Ordenamento de ítens
    Route::post('ordenar', 'App\Controllers\Painel\OrdenadorController@ordenar');

    //Upload de fotos em projetos
    Route::resource('projetofotos', 'App\Controllers\Painel\ProjetoFotosController');
    //Upload de fotos em mídias
    Route::resource('midiafotos', 'App\Controllers\Painel\MidiaFotosController');
});

/**
 * Rota da home
 */
Route::get('/', function(){
    //$slides = Cache::get('slides');
    $slides = Slide::ordered()->get();

    //$destaques = Cache::get('projetosDestacados')->toArray();
    $destaques = Projeto::where('destaque', true)->ordered()->with('categoria')->get()->toArray();
    $k = array_rand($destaques);
    $destaque = $destaques[$k];

    return View::make('site.home.index', compact('slides', 'destaque'));
});

/**
 * Rotas do front-end
 */

/**
 * Empresa (view por rota)
 */
Route::get('empresa', function(){
    $empresa = Pagina::where('slug', 'empresa')->first();

    return View::make('site.empresa', compact('empresa'));
});

/**
 * Projetos (view por controller)
 */
Route::get('projetos', 'App\Controllers\Site\ProjetosController@categorias');
Route::get('projetos/{categoria_slug}',
        array('as' => 'projetos', 'uses' => 'App\Controllers\Site\ProjetosController@categoria'));
Route::get('projetos/{categoria_slug}/{projeto_slug}',
        array('as' => 'projeto', 'uses' => 'App\Controllers\Site\ProjetosController@projeto'));

/**
 * Publicações (view por rota)
 */
Route::get('publicacoes', function(){
    $publicacoes = Midia::all();

    return View::make('site.publicacoes', compact('publicacoes'));
});

/**
 * Profissionais (view por controller / ajax post)
 */
Route::get('profissionais', 'App\Controllers\Site\ProfissionaisController@index');
Route::post('profissionais', 'App\Controllers\Painel\ProfissionalsController@store');

/**
 * Contato (view por controller / ajax post)
 */
Route::get('contato', 'App\Controllers\Site\ContatoController@index');
Route::post('contato', 'App\Controllers\Site\ContatoController@sendMail');

/**
 * Envio de currículos
 */
Route::post('curriculos', 'App\Controllers\Site\ContatoController@curriculos');

/**
 * Composers
 */
View::composer('site.layout.default', 'App\Composers\ContactsComposer');
View::composer('site.layout.default', 'App\Composers\SocialsComposer');
View::composer('site.layout.default', 'App\Composers\CategoriasComposer');

Route::get('teste', 'App\Controllers\Painel\ProjetoFotosController@index');
