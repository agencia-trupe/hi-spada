@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-empresa">
        <div class="interna">
            <h1 class="titulo-pagina">Empresa</h1>
            <div class="separador-titulo"></div>
            <div class="clearfix"></div>
            <div class="texto-empresa">
                {{ $empresa->texto }}
            </div>
            <div class="imagem-empresa">
                <img src="{{ URL::asset(Image::resize($empresa->imagem, 400, 605))}}" alt="Hi-Spada - Movelaira e Design">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop
