@extends('painel._layouts.default')

@section('main')

<h1>Editar informações de contato</h1>
{{ Form::model($local, array('files' => true, 'method' => 'PATCH', 'route' => array('painel.locais.update', $local->id))) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('endereco') ? 'error' : '' }}">
        {{ Form::label('endereco', 'Endereço:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('endereco', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('endereco') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('numero') ? 'error' : '' }}">
        {{ Form::label('numero', 'Número:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('numero', NULL, array('class' => 'input-small')) }}
        </div>
        <h5>{{ $errors->first('numero') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('complemento', 'Complemento:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('complemento') }}
        </div>
        <h5>{{ $errors->first('complemento') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('bairro') ? 'error' : '' }}">
        {{ Form::label('bairro', 'Bairro:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('bairro') }}
        </div>
        <h5>{{ $errors->first('bairro') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('cidade') ? 'error' : '' }}">
        {{ Form::label('cidade', 'Cidade:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('cidade') }}
        </div>
        <h5>{{ $errors->first('cidade') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('cep') ? 'error' : '' }}">
        {{ Form::label('cep', 'CEP:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('cep') }}
        </div>
        <h5>{{ $errors->first('cep') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('uf') ? 'error' : '' }}">
        {{ Form::label('uf', 'UF:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('uf', null, array('class' => 'input-small ')) }}
        </div>
        <h5>{{ $errors->first('uf') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('ddd1') ? 'error' : '' }}">
        {{ Form::label('ddd1', 'DDD 1:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('ddd1', null, array('class' => 'input-small')) }}
        </div>
        <h5>{{ $errors->first('ddd1') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('telefone1') ? 'error' : '' }}">
        {{ Form::label('telefone1', 'Telefone 1:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('telefone1') }}
        </div>
        <h5>{{ $errors->first('telefone1') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Email:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email') }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.locais.show', 'Cancelar', $local->id, array('class' => 'btn')) }}
{{ Form::close() }}

@stop
