@extends('site.layout.default')

@section('main')
    <div class="interna">
        <h1>404 Não encontrado</h1>
        <p>A página solicitada não pode ser encontrada.</p>
        <div class="clearfix"></div>
    </div>
@stop
