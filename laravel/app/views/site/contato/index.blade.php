@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-contato">
        <div class="interna">
            <h1>Contato</h1>
            <div class="clearfix"></div>
            <div class="form-contato">
                <h2>Envie uma mensagem</h2>
                {{ Form::open(array('id'=>'form-contato')) }}
                    <div class="input">
                        {{ Form::text('nome', null, array('placeholder' => 'nome')) }}
                    </div>
                    <div class="input">
                        {{ Form::text('email', null, array('placeholder' => 'email')) }}
                    </div>
                    <div class="textarea">
                        <span class="legenda mensagem">mensagem</span>
                        {{ Form::textarea('mensagem', null, array('id' => 'mensagem')) }}
                    </div>
                    {{ Form::submit('enviar') }}
                {{ Form::close()}}
            </div>
            <div class="form-trabalhe">
                <h2>Trabalhe conosco</h2>
                {{ Form::open(array('id' => 'form-trabalhe', 'data-send-url' => 'curriculos', 'files' => true)) }}
                    <div class="input">
                        {{ Form::text('nome', null, array('placeholder' => 'nome')) }}
                    </div>
                    <div class="input">
                        {{ Form::text('email', null, array('placeholder' => 'email')) }}
                    </div>
                    <div class="file">
                        {{ Form::file('curriculo', array('id' => 'curriculo')) }}
                    </div>
                    {{ Form::submit('enviar') }}
                {{ Form::close() }}
            </div>
            <div class="contato-contatos">
                <h2>Contatos</h2>
                <div class="contato-telefone">
                    <span>{{ $contato->ddd1 }} {{ $contato->telefone1 }}</span>
                </div>
                <div class="contato-email">
                    <span>{{ $contato->email }}</span>
                </div>
                <h2>Localização</h2>
                <p class="localizacao">
                    {{ $contato->endereco }}, {{ $contato->numero }}, {{ $contato->bairro }} - {{ $contato->cidade }}, {{ $contato->uf }}<br>
                    CEP: {{ $contato->cep }}
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop
