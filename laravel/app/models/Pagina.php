<?php

class Pagina extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'user_id' => 'required'
    );
}
