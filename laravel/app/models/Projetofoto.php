<?php

class ProjetoFoto extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'projeto_id' => 'required');

    public function projetos()
    {
        return $this->belongsTo('Projeto');
    }
}
