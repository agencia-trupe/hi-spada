<?php namespace App\Services\Validators;

class CurriculoValidator extends Validator
{
    public static $rules = array(
        'nome' => 'required',
        'email' => 'required|email',
    );
}
