@extends('site.layout.default')

@section('main')
<div class="interna">
    <div class="slides-home-wrapper">
        <div class="home-slider">
            @foreach ($slides as $slide)
                <div class="slide">
                    <a href="{{ $slide->link }}">
                        <img src="{{ URL::asset($slide->imagem) }}" alt="{{ $slide->titulo }}">
                        <div class="image-hover"></div>
                        <span class="clearfix"></span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="home-destaque">
        <div class="clearfix"></div>
        <a href="{{ URL::to('projetos/' . $destaque['categoria']['slug'] . '/' . $destaque['slug'] ) }}" class="home-destaque-imagem">
            <img src="{{ URL::asset(Image::thumb($destaque['imagem'], 350, 300)) }}" alt="{{ $destaque['titulo'] }}">
            <div class="image-hover"></div>
        </a>
        <div class="separador-vertical"></div>
        <div class="home-destaque-texto">
            <div class="home-destaque-titulo">
                <h1>{{ $destaque['titulo'] }}</h1>
            </div>
            <div class="home-destaque-descricao">
                @if ( strlen($destaque['descricao'] > 260 ) ) {{ substr($destaque['descricao'], 0, strpos($destaque['descricao'], ' ', 260)) }}
                @else {{ $destaque['descricao'] }}
                @endif
            </div>
            <a href="{{ URL::to('projetos/' . $destaque['categoria']['slug'] . '/' . $destaque['slug'] )}}" class="home-destaque-detalhes">detalhes +</a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@stop
