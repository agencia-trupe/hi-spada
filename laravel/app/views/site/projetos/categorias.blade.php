@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-projetos conteudo-categorias">
        <div class="interna">
            <h1>Projetos</h1>
            <div class="separador-titulo"></div>
            <div class="separador-mais"></div>
            <div class="clearfix"></div>
            <div class="categorias-links">
                @foreach ($categorias as $categoria)
                    <a href="{{ URL::route('projetos', $categoria->slug) }}" class="categoria-link">
                        <img src="{{ URL::asset(Image::thumb($categoria->imagem, 170, 460)) }}" alt="Hispada - Projetos - {{ $categoria->titulo }}">
                        <span class="categoria-overlay">
                            <span class="categoria-titulo">{{ $categoria->titulo }}</span>
                        </span>
                        <span class="image-hover"></span>
                    </a>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop
