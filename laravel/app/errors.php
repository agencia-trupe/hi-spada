<?php
/**
 * errors.php
 *
 * Functions for error handling in laravel
 *
 * @package     Laravel
 * @category    Errors
 * @author      Nilton Freitas
 * @link        http://nilton.name
 * @version     0.1
 * @todo        -
 */

App::missing( function($exception){
    return Response::view('site.error404', array(), 404);
});
