@extends('painel._layouts.default')

@section('main')
<h1>Editar Publicação</h1>
<div class="row-fluid">
    <div class="span7">
        {{ Form::model($midia, array('method' => 'PATCH', 'route' => array('painel.midias.update', $midia->id), 'files' => TRUE, 'data-model-name' => 'midia', 'class' => 'related')) }}
            {{ Form::hidden('object_id', $midia->id) }}
            <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
                {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
                </div>
                <h5>{{ $errors->first('titulo') }}<h5>
            </div>
             <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
                {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
                </div>
                <h5>{{ $errors->first('slug') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
                {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
                </div>
                <h5>{{ $errors->first('descricao') }}<h5>
            </div>
            <div class="control-group">
                {{ Form::label('imagem', 'Imagem em destaque') }}

                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                        @if ($midia->imagem)
                            <a href="<?php echo $midia->imagem; ?>"><img src="<?php echo asset($midia->imagem); ?>" alt=""></a>
                        @else
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                        @endif
                    </div>
                    <div>
                        <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
                    </div>
                </div>
            </div>

            {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
            {{ link_to_route('painel.midias.show', 'Cancelar', $midia->id, array('class' => 'btn')) }}
        {{ Form::close() }}
    </div>
    <div class="span5 well">
        <h2>Fotos</h2>
        <hr>
        {{ Form::open(array('files' => true, 'id' => 'related_file', 'data-send-url' => '/painel/midiafotos')) }}
            {{ Form::file('upload', array('id' => 'upload')) }}
            <br>
            {{ Form::submit('Adicionar foto', array('class="btn btn-info btn-small"'))}}
        {{ Form::close() }}
        <hr>
        @if ($midia->midiafotos)
            <a href="#" class="btn btn-info btn-mini ordenar" data-model="midiafoto">Ordenar ítens</a>
            <br>
            <br>
        @endif
        <div class="related-files-list sortable">
            @foreach ($midia->midiafotos as $foto)
                <div id="midiafoto_{{ $foto->id }}" class="related_file_wrapper" data-id="{{ $foto->id }}">
                    <img style="height:100px !important" src="{{ URL::asset($foto->imagem)}}"
                                width="100px" height="100px" />
                    <a style="margin-top:10px" class="btn btn-mini btn-danger">
                        <i class="iconic-trash"></i>apagar
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

@stop
