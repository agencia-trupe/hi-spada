@extends('painel._layouts.default')

@section('main')

<h1>Slides</h1>

{{ link_to_route('painel.slides.create', 'Novo slide', null, array('class' => 'btn btn-mini btn-info')) }}
  <a href="#" data-model="slide" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($slides->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Link</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody class="sortable">
            @foreach ($slides as $slide)
                <tr id="slide_{{ $slide->id }}">
                    <td>{{{ $slide->titulo }}}</td>
                    <td>{{{ $slide->link }}}</td>
                    <td>
                        @if ( $slide->imagem )
                            <img width="200" src="{{ URL::asset($slide->imagem) }}" alt="{{ $slide->titulo }}">
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($slide->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.slides.edit', 'Editar', array($slide->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.slides.destroy', $slide->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="lead">
        Nenhum slide cadastrado.
    </p>
@endif

@stop
