@extends('painel._layouts.default')

@section('main')

<h1>Nova Rede Social</h1>

{{ Form::open(array('files' => TRUE, 'route' => 'painel.socials.store')) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('link') ? 'error' : '' }}">
        {{ Form::label('link', 'Link:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('link', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('link') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 16px; height: 16px;">
                <img src="http://www.placehold.it/24x24/EFEFEF/AAAAAA">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
