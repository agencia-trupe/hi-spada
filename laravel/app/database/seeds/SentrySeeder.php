<?php

use App\Models\User;

class SentrySeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('groups')->delete();
        DB::table('users_groups')->delete();

        Sentry::getUserProvider()->create(array(
            'username'    => 'trupe',
            'email'       => 'nilton@trupe.net',
            'password'    => "senhatrupe",
            'first_name'  => 'Trupe',
            'last_name'   => 'Agência Criativa',
            'activated'   => 1,
        ));

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Admin',
            'permissions' => array('admin' => 1),
        ));

        // Assign user permissions
        $adminUser  = Sentry::getUserProvider()->findByLogin('trupe');
        $adminGroup = Sentry::getGroupProvider()->findByName('Admin');
        $adminUser->addGroup($adminGroup);
    }

}
