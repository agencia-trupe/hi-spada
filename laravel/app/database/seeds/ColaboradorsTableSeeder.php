<?php

class ColaboradorsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('colaboradors')->truncate();

		$colaboradors = array(
			[
				'nome' 		=>	'Svante Hjorth',
				'email' 	=>	'svante.hjorth@arycom.com',
				'descricao'	=>	'<p>Svante Hjorth, CEO da Arycom é sueco e vive
				há mais de 15 anos no Brasil. Tem 17 anos de experência em
				estratégia de vendas, desenvolvimento de negócios e engenharia
				de TI e Telecomunicações, tendo atuado peincipalmente em 
				empresas estrageiras e estabelecendo operações na América 
				Latina.</p>',
				'user_id'	=> 2
			]
		);

		// Uncomment the below to run the seeder
		DB::table('colaboradors')->insert($colaboradors);
	}

}
