@extends('painel._layouts.default')

@section('main')

<h1>Paginas</h1>

<p>{{ link_to_route('painel.paginas.create', 'Nova página', null, array('class' => 'btn btn-mini btn-info')) }}
<a href="#" data-model="projeto" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($paginas->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Texto</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($paginas as $pagina)
                <tr>
                    <td>{{{ $pagina->titulo }}}</td>
                    <td>{{{ $pagina->slug }}}</td>
                    <td>{{ $pagina->resumo }}</td>
                    <td>{{ $pagina->texto }}</td>
                    <td>
                        @if ( $pagina->imagem )
                            <img src="{{ URL::asset($pagina->imagem) }}" alt="{{ $pagina->titulo }}">
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($pagina->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.paginas.edit', 'Editar', array($pagina->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.paginas.destroy', $pagina->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="lead">
        Nenhuma página cadastrada.
    </p>
@endif

@stop
