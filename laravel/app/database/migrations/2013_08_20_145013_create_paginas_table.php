<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaginasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paginas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->unique();
            $table->text('resumo');
            $table->text('texto');
            $table->string('imagem')->nullable();
            $table->string('template');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paginas');
    }

}
