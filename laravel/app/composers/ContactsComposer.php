<?php namespace App\Composers;

/**
 * ContactsComposer.php
 *
 * ContactsComposer
 *
 * Share contact data with layout view.
 *
 * @package     Arycom
 * @category    Composers
 * @framework   Laravel
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use Local;

class ContactsComposer
{
    public function compose($view)
    {
        $contato = Local::ordered()->first();
        $view->with('contato', $contato);
    }

}
