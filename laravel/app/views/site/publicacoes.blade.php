@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-publicacoes">
        <div class="interna">
            <h1>Publicações</h1>
            <div class="publicacoes-lista">
                @foreach ($publicacoes as $publicacao)
                    <a href="#" role="lightbox" data-shadow-id="{{ $publicacao->id }}" class="publicacao-link">
                        <img class="gray" src="{{ URL::asset(Image::thumb($publicacao->imagem, 214, 280)) }}" alt="">
                        <span class="image-hover"></span>
                    </a>
                @endforeach
                <div class="clearfix"></div>
            </div>
            @foreach ($publicacoes as $publicacao)
                <div data-lightbox-id="{{ $publicacao->id }}" class="nlightbox-content-wrapper">
                    <a class="nlightbox-close" href="#">Close</a>
                    <div class="clearfix"></div>
                    <div class="publicacoes-lightbox-slider-wrapper">
                        <div class="publicacoes-lightbox-slider {{ (count($publicacao->midiafotos) > 2) ? 'multiple' : 'single'}}">
                            @foreach (array_chunk($publicacao->midiafotos->toarray(), 2) as $slides)
                                <div class="publicacoes-lightbox-slide">
                                    @foreach ($slides as $slide)
                                        <img src="{{ URL::asset(Image::thumb($slide['imagem'], 470, 615)) }}" alt="">
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="nlightbox-overlay"></div>
        </div>
    </div>
@stop
