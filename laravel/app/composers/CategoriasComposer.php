<?php namespace App\Composers;

/**
 * CategoriasComposer.php
 *
 * CategoriasComposer
 *
 * Compartilha os dados de redes sociais com a view de layout.
 *
 * @package     Arycom
 * @category    Composers
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.0.1
 * @todo        -
 */

use Cache;

class CategoriasComposer
{
    public function compose($view)
    {
        $categorias = Cache::get('categorias');
        $view->with('navCategorias', $categorias);
    }

}
