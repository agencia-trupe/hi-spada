@extends('painel._layouts.default')

@section('main')

<h1>Categorias</h1>

<p>{{ link_to_route('painel.categorias.create', 'Nova categoria', null, array('class' => 'btn btn-mini btn-info')) }}
 <a href="#" data-model="categoria" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($categorias->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Texto</th>
                <th>Criado por</th>
                <th class="span2"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody class="sortable">
            @foreach ($categorias as $categoria)
                <tr id="categoria_{{$categoria->id}}">
                    <td>{{{ $categoria->titulo }}}</td>
                    <td>{{{ $categoria->slug }}}</td>
                    <td>{{ $categoria->descricao }}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($categoria->user_id)->first_name }}}</td>
                    <td>
                        <a class="btn btn-info btn-mini" href="{{ URL::to('painel/projetos?categoria_id=' . $categoria->id) }}">Listar projetos</a>
                        {{ link_to_route('painel.categorias.edit', 'Editar categoria', array($categoria->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.categorias.destroy', $categoria->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="lead">
        Nenhuma categoria cadastrada.
    </p>
@endif

@stop
