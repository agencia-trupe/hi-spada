<?php

class Local extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'endereco' => 'required',
        'numero' => 'required',
        'bairro' => 'required',
        'cidade' => 'required',
        'cep' => 'required',
        'uf' => 'required',
        'email' => 'required',
    );
}
