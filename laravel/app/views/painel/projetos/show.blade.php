@extends('painel._layouts.default')

@section('main')

<h1>Projeto</h1>

<p>{{ link_to_route('painel.projetos.index', 'Listar projetos', array('categoria_id' => $projeto->categoria_id), array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
            <tr>
                <th>Categoria</th>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{ $projeto->categoria->titulo }}</td>
            <td>{{{ $projeto->titulo }}}</td>
            <td>{{{ $projeto->slug }}}</td>
            <td>
                @if ( strlen($projeto->descricao) > 100) {{ substr($projeto->descricao, 0, 100) }}[...]
                @else {{ $projeto->descricao }}
                @endif
            </td>
            <td>{{{ Sentry::getUserProvider()->findById($projeto->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.projetos.edit', 'Editar', array($projeto->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.projetos.destroy', $projeto->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
