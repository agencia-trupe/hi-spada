<?php

class Social extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'link' => 'required',
    );
}
