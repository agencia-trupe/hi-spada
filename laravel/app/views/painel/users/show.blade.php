@extends('painel._layouts.default')

@section('main')

<h1>{{ $user->titulo }}</h1>

<p>{{ link_to_route('painel.users.index', 'Listar redes sociais', NULL, array('class'=>'btn btn-mini btn-info')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
                <th>Link</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $user->titulo }}}</td>
                    <td>{{{ $user->link }}}</td>
                    <td><img src="{{ URL::asset( $user->imagem ) }}" alt="{{ $user->titulo }}"></td>
                    <td>{{ Sentry::getUserProvider()->findById($user->user_id)->first_name }}</td>
                    <td>{{ link_to_route('painel.users.edit', 'Editar', array($user->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.users.destroy', $user->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
