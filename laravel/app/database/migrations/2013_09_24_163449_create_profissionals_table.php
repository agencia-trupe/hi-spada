<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfissionalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profissionals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->string('telefone');
			$table->string('email');
			$table->string('endereco');
			$table->string('site');
			$table->string('cau');
			$table->string('crea');
			$table->string('abd');
			$table->integer('ordem');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profissionals');
	}

}
