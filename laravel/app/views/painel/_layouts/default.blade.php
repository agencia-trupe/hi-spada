<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Hi-Spada</title>

    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/jasny-bootstrap-responsive.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.min.css">
    <style>
        body{
            padding-top: 60px;
        }
        .control-group{
            max-width:600px;
        }
    </style>
</head>
<body>
<div>
    @if (Sentry::check())
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="{{ URL::to('/') }}">Hi-Spada</a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="{{ Request::is('*paginas*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.paginas.index')}}">Paginas</a></li>
                        <li class="{{ Request::is('*categorias*') || Request::is('*projetos*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.categorias.index')}}">Projetos</a></li>
                        <li class="{{ Request::is('*midias*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.midias.index')}}">Publicações</a></li>
                        <li class="{{ Request::is('*profissionais*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.profissionais.index')}}">Profissionais</a></li>
                        <li class="{{ Request::is('*socials*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.socials.index')}}">Redes Sociais</a></li>
                        <li class="{{ Request::is('*slides*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.slides.index')}}">Slides</a></li>
                        <li class="{{ Request::is('*locais*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.locais.edit', 1)}}">Contato</a></li>
                        <li class="{{ Request::is('*users*') ? 'active' : '' }}">
                            <a href="{{ URL::route('painel.users.index')}}">Usuários</a></li>
                    </ul>
                    <ul class="nav pull-right">
                        <li>
                            <a href="{{ URL::route('painel.logout') }}" class="pull-right">Logout</a>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
    @endif

    <div class="container">
            @if (Session::has('message'))
                <div class="flash alert">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif

            @yield('main')
        </div>
</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ URL::asset('assets/js/vendor/jquery-1.9.1.min.js') }}"><\/script>')</script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js' )}}"></script>
    <script src="{{ URL::asset('assets/js/jasny-bootstrap.min.js' )}}"></script>
    <script src="{{ URL::asset('assets/ckeditor/ckeditor.js' )}}"></script>
    <script src="{{ URL::asset('assets/ckeditor/adapters/jquery.js' )}}"></script>
    <script src="{{ URL::asset('assets/js/plugins.js' )}}"></script>
    <script src="{{ URL::asset('assets/js/admin1.js' )}}"></script>
    <script>
        function string_to_slug(str)
        {
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "ãàáäâèéëêìíïîõòóöôùúüûñç·/_,:;";
          var to   = "aaaaaeeeeiiiiooooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

          return str;
        }

        var title = $('#titulo'),
        slug = $('#slug');

        title.on('keyup', function() {
          var val = $(this).val();
          val = string_to_slug(val);
          slug.val(val);
        });

    </script>
</body>
</html>
