@extends('painel._layouts.default')

@section('main')

<h1>Criar Profissional</h1>

{{ Form::open(array('route' => 'painel.profissionais.store', 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('nome') ? 'error' : '' }}">
        {{ Form::label('nome', 'Nome:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('nome', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('nome') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('telefone') ? 'error' : '' }}">
        {{ Form::label('telefone', 'Telefone:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('telefone', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('telefone') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Email:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('endereco') ? 'error' : '' }}">
        {{ Form::label('endereco', 'Endereço:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('endereco', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('endereco') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('site') ? 'error' : '' }}">
        {{ Form::label('site', 'Site:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('site', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('site') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('cau') ? 'error' : '' }}">
        {{ Form::label('cau', 'Nº CAU:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('cau', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('cau') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('crea') ? 'error' : '' }}">
        {{ Form::label('crea', 'Nº CREA:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('crea', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('crea') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('abd') ? 'error' : '' }}">
        {{ Form::label('abd', 'Nº ABD:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('abd', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('abd') }}<h5>
    </div>
        {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
