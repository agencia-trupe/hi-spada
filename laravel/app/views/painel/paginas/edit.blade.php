@extends('painel._layouts.default')

@section('main')

<h1>Editar Pagina</h1>
{{ Form::model($pagina, array('method' => 'PATCH', 'route' => array('painel.paginas.update', $pagina->id), 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
     <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>

    <div class="control-group {{ $errors->first('texto') ? 'error' : '' }}">
        {{ Form::label('texto', 'Texto:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('texto', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('texto') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                @if ($pagina->imagem)
                    <a href="<?php echo $pagina->imagem; ?>"><img src="<?php echo asset($pagina->imagem); ?>" alt=""></a>
                @else
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                @endif
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
            </div>
        </div>
    </div>

    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.paginas.show', 'Cancelar', $pagina->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
