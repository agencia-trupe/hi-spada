<?php

class MidiaFoto extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'midia_id' => 'required');

    public function midias()
    {
        return $this->belongsTo('Midia');
    }
}
