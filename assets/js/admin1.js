//Defnição de prefixo de url para diferentes ambientes
var environment = "testing";
if (environment == 'development') {
	var prefix = "";
} else if (environment == 'testing') {
	var prefix = ""; 
} else {
	var prefix = "";
}

/**
 * Retorna o caminho completo para o segmento passado como parâmetro.
 */
function baseUrl(urlToAppend){
	return location.protocol + "//" + location.hostname + prefix + urlToAppend
}

/**
 * Ckeditor config object
 */
var config = {
	height: 180,
    width: 515,
    toolbar: 'Basic',
    linkShowAdvancedTab: false,
    scayt_autoStartup: true,
    toolbar_Basic: [['Format', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']]
}
/**
 * Chamada para o ckeditor
 */
$('.ceditor').ckeditor(config);


//Seletor de tipo de mídia
$('#midia-form #midiatipo_id').change(function(){
	if( $(this).val() === '1' )
	{
		$('#midia-form #video-controls').hide();
		$('#midia-form #imagem-controls').show();
	} else {
		$('#midia-form #video-controls').show();
		$('#midia-form #imagem-controls').hide();
	}
});

//Datepicker
if($(".datepicker").val() === ''){
	var datepicker = $( ".datepicker" ).datepicker({
		dateFormat: "dd/mm/yy"
	}).datepicker("setDate", new Date());
} else {
	var datepicker = $( ".datepicker" ).datepicker({
		dateFormat: "dd/mm/yy"
	});
}

$(".datepicker[value='']")
$(".datesetter").click(function(){
	datepicker.datepicker("show");
});

//Timepicker
var timepicker = $(".timepicker").timepicker();

$(".timesetter").click(function(){
	timepicker.timepicker("show");
});

/*==============================================================================
	Upload de arquivos relacionados a modelos
  ============================================================================*/
/**
* Upload de fotos para projetos
*/
$('#related_file').submit(function(e) {
  		if($('[name="object_id"]').val() == "")
  		{
  			$.pnotify({
			    title: 'Erro!',
			    text: 'Corrija os erros no formulário de cadastro antes de adicionar uma foto',
			    type: 'error'
			});
			e.preventDefault();
  		} else {
		    e.preventDefault();
		    $.ajaxFileUpload({
		        url         	: $(this).data('send-url'),
		        secureuri      	: false,
		        fileElementId  	: 'upload',
		        dataType    	: "JSON",
		        accepts: {
				  	json: 'application/json'
				},
		        data        : {
		            'object_id': $('[name="object_id"]').val(),
		            'model': $('.related').data('model-name')
		        },
		        success  : function (data)
		        {
		            data = JSON.parse(data);
		            if(data.errors)
		            {
		            	$.pnotify({
						    title: 'Erro!',
						    text: data.errors.message,
						    type: 'error'
						});
		            } else {
		            	var projetoFotoTemplate = '<div class="related_file_wrapper" data-id="' + data.id + '">'
		            							+ '<img style="height:100px !important" src="' + data.path + '" width="100px" height="100px" />' 
		            							+ '<a style="margin-top:10px" class="btn btn-mini btn-danger"><i class="iconic-trash"></i>apagar</a></div>';
		            	$('.related-files-list').append(projetoFotoTemplate);
		            }
		        }
		    });
		    return false;
		}
});

/**
 * Apagar foto de projetos
 */
$(document.body).on('click', '.related_file_wrapper a', function(){
	var id = $(this).parent().data("id");

	$.ajax({
		url: baseUrl($('#related_file').data('send-url')) + '/' + id,
		type: 'POST',
		dataType: 'json',
		data: {
			_method: 'DELETE',
		},
	})
	.done(function() {
		$.pnotify({
		    title: 'Arquivo removido com sucesso!',
		    text: '',
		    type: 'success'
		});
		$('[data-id="' + id + '"]').remove();
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	return false;
});

/*==============================================================================
	Ordenar ítens
  ============================================================================*/

$('.ordenar').click(function(){
	if($(this).hasClass('salvar-ordem')){
		$.ajax({
			url: 'ordenar',
			type: 'POST',
			dataType: 'json',
			data: {
				model: $(this).data('model'),
				itens: $(".sortable").sortable("serialize")
			},
		})
		.done(function(data) {
			$('.ordenar').removeClass('btn-sucess salvar-ordem').addClass('btn-info').text('Ordenar ítens');
			$(".sortable").sortable({'disabled': false, 'delay': '100'});
			alert(data.message);
		})
		.fail(function() {
			console.log("error");
		})
	} else {
		$(".sortable").sortable({'disabled': false, 'delay': '100'});
		$(this).removeClass('btn-info').addClass('salvar-ordem btn-success').text('Salvar Ordem');
	}
	return false;
});