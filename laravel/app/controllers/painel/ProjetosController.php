<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Categoria, Image, Projeto;

class ProjetosController extends BaseController
{
    /**
     * Categoria Repository
     *
     * @var Categoria
     */
    protected $projeto;

    public function __construct(Projeto $projeto)
    {
        $this->projeto = $projeto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Input::get('categoria_id')) {
            $projetos = $this->projeto
                        ->where('categoria_id', Input::get('categoria_id'))
                        ->ordered()
                        ->get();
        } else {
            $projetos = $this->projeto->all();
        }

        return View::make('painel.projetos.index', compact('projetos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //Array com categorias disponíveis e seus respectivos ids como chave.
        $categorias = array('' => 'Selecione uma categoria') + Categoria::lists('titulo', 'id');

        return View::make('painel.projetos.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), array('_method', 'object_id'));;
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Projeto::$rules);

        if ($validation->passes()) {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'projetos/destaque');
            }

            $projeto = new Projeto($input);
            $projeto->save();

            return Redirect::route('painel.projetos.edit', $projeto->id);
        }

        if (Input::ajax()) {
            return json_encode(array('errors' => $validation->errors()->toArray()));
        }

        return Redirect::route('painel.projetos.create')
            ->withInput()
            ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $projeto = $this->projeto->findOrFail($id);

        return View::make('painel.projetos.show', compact('projeto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        //Array com categorias disponíveis e seus respectivos ids como chave.
        $categorias = array('' => 'Selecione uma categoria') + Categoria::lists('titulo', 'id');
        $projeto = $this->projeto->find($id);

        if (is_null($projeto)) {
            return Redirect::route('painel.projetos.index');
        }

        return View::make('painel.projetos.edit', compact('projeto', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {

        $input = array_except(Input::all(), array('_method', 'object_id', 'imagem'));
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Projeto::$rules);

        if ($validation->passes()) {
            $projeto = $this->projeto->find($id);

             if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'projetos/destaque');
                @unlink(public_path(substr($projeto->imagem, 1)));
            }
            if( !isset($input['destaque']) )
                $input['destaque'] = false;

            $projeto->update($input);

            return Redirect::route('painel.projetos.show', $id);
        }

        if (Input::ajax()) {
            return json_encode(array('errors' => $validation->errors()->toArray(), 'projeto_id' => $id));
        }

        return Redirect::route('painel.projetos.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $projeto = $this->projeto->find($id);

        @unlink(public_path(substr($projeto->imagem, 1)));

        $projeto->delete();

        return Redirect::route('painel.projetos.index');
    }

}
