<?php namespace App\Controllers\Site;

use View;
use Categoria;
use Projeto;

class ProjetosController extends \BaseController
{
    public function categorias()
    {
        $categorias = Categoria::orderBy('ordem', 'asc')->get();

        return View::make('site.projetos.categorias', compact('categorias'));
    }

    public function categoria( $categoria_slug )
    {
        $categoria = Categoria::where('slug', $categoria_slug)->first();

        return View::make('site.projetos.categoria', compact('categoria'));
    }

    public function projeto( $categoria_slug, $projeto_slug )
    {
        $categoria = Categoria::where('slug', $categoria_slug)->first();

        $projeto = Projeto::where('slug', $projeto_slug)->where('categoria_id', $categoria->id )->first();
        $tabsIndex = $projeto->projetofotos->toArray();
        for ($i=0; $i < count($tabsIndex); $i++) {
            $tabsIndex[$i]['slide_index'] = $i;
        }
        $tabsIndex = array_chunk($tabsIndex, 8);

        return View::make('site.projetos.projeto', compact('projeto', 'tabsIndex'));
    }
}
