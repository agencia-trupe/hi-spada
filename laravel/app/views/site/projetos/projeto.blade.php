@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-projetos conteudo-projeto">
        <div class="interna">
            <h1>Projetos <span class="separador">|</span> {{ strtolower( $projeto->categoria->titulo )}}</h1>
            <div class="separador-titulo"></div>
            <div class="separador-mais"></div>
            <div class="clearfix"></div>
            <div class="projeto-tabs-wrapper">
                <h2 class="projeto-titulo">{{ $projeto->titulo }}</h2>
                <div class="projeto-descricao">{{$projeto->descricao}}</div>
                <div class="projeto-tabs">
                    <div class="projeto-tabs-main-slider">
                        @foreach ($projeto->projetofotos as $foto)
                            <div class="projeto-tab" id="projeto-tab-{{ $foto->id }}">
                                <img src="{{ URL::asset(Image::resize($foto->imagem, 690, 460)) }}" alt="{{ $projeto->titulo }}">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="projeto-tabs-index-wrapper">
                    <div class="projeto-tabs-index">
                        @foreach ($tabsIndex as $index)
                            <div class="projeto-tabs-index-page">
                                <ul>
                                    @foreach ($index as $link)
                                        <li>
                                            <a data-slide-index="{{ $link['slide_index'] }}" href="">
                                                <img src="{{ URL::asset(Image::thumb($link['imagem'], 110, 90)) }}" alt="">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop
