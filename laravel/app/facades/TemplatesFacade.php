<?php namespace App\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class TemplatesFacade extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return new \App\Services\Templates;
    }
 
}