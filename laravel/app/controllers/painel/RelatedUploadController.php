<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Categoria, Image, Projeto, ProjetoFoto, Response;

class RelatedUploadController extends BaseController
{
    /**
     * ProjetoFoto repository
     *
     */
    public $model;

    /**
     * ProjetoFotos constructor
     *
     * @param ProjetoFoto $projetoFoto Instância do repositório ProjetoFoto
     */
    public function __construct($modelInstance)
    {
        $this->model = $modelInstance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->model->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), array('upload'));
        $input['user_id'] = Sentry::getUser()->id;
        $input[$input['model'] . '_id'] = $input['object_id'];

        if ( ! Input::hasFile('upload') ) {
            return json_encode(
                array(
                'errors' =>
                    array(
                'title' => 'emptyFilePayload',
                'message' => 'Você precisa selecionar um arquivo para envio')
                ));
        }

        $input['imagem'] = Image::upload(Input::file('upload'), strtolower(get_class($this->model)));
        $object = new $this->model(array_except($input, array('object_id', 'model')));
        $object->save();

        return json_encode(array(
            'errors' => false,
            'success' => 'Arquivo adicionado com sucesso',
            'path' => $input['imagem'],
            'id' => $object->id));

    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $object = $this->model->find($id);
        $object->delete();

        return json_encode(array('errors' => false, 'success' => 'Arquivo apagado com sucesso'));
    }

}
