@extends('painel._layouts.default')

@section('main')

<h1>Slide</h1>

<p>{{ link_to_route('painel.slides.index', 'Listar slides', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
   <thead>
        <tr>
            <th>Titulo</th>
            <th>Link</th>
            <th>Imagem</th>
            <th>Criado por</th>
            <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $slide->titulo }}}</td>
            <td>{{{ $slide->link }}}</td>
            <td>
                @if ( $slide->imagem )
                    <img width="200" src="{{ URL::asset($slide->imagem) }}" alt="{{ $slide->titulo }}">
                @endif
            </td>
            <td>{{{ Sentry::getUserProvider()->findById($slide->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.slides.edit', 'Editar', array($slide->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.slides.destroy', $slide->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
