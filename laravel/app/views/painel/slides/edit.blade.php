@extends('painel._layouts.default')

@section('main')

<h1>Editar Pagina</h1>
{{ Form::model($slide, array('method' => 'PATCH', 'route' => array('painel.slides.update', $slide->id), 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
     <div class="control-group {{ $errors->first('link') ? 'error' : '' }}">
        {{ Form::label('link', 'Link:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('link', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('link') }}<h5>
    </div>
    <div class="control-group">
    {{ Form::label('imagem', 'Imagem') }}

    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
            @if ($slide->imagem)
                <a href="<?php echo $slide->imagem; ?>"><img src="<?php echo asset($slide->imagem); ?>" alt=""></a>
            @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
</div>

    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.slides.show', 'Cancelar', $slide->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
