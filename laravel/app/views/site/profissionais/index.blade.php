@extends('site.layout.default')

@section('main')
    <div class="conteudo-pagina conteudo-profissionais">
        <div class="interna">
            <h1>Profissionais</h1>
            <div class="clearfix"></div>
            <div class="profissionais-conteudo">
                <div class="descricao-profissionais">
                    <h2>Torne-se um profissional indicado pela Hi-Spada</h2>
                    <p>Profissionais de arquitetura e decoradores, cadastrem-se
                     ao lado em nosso site e façam parte dos profissionais indicados</p>
                </div>
                <div class="form-profissionais">
                    {{ Form::open(array('id' => 'profissionais-form'))}}
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('nome', 'Nome') }}
                            {{ Form::text('nome') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('telefone', 'Telefone') }}
                            {{ Form::text('telefone') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('email', 'E-mail') }}
                            {{ Form::text('email') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('endereco', 'Endereço') }}
                            {{ Form::text('endereco') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('site', 'Site') }}
                            {{ Form::text('site') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('cau', 'Nº CAU') }}
                            {{ Form::text('cau') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('crea', 'CREA') }}
                            {{ Form::text('crea') }}
                        </div>
                        <div class="input">
                            <div class="clearfix"></div>
                            {{ Form::label('abd', 'ABD') }}
                            {{ Form::text('abd') }}
                        </div>
                        {{ Form::submit('Enviar') }}
                    {{ Form::close() }}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop
