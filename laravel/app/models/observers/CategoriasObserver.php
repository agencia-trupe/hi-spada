<?php

class CategoriasObserver
{
    public function saved($model)
    {
        $categorias = Categoria::select('titulo', 'slug')->ordered()->get();
        Cache::put('categorias', $categorias, 0);
    }

    public function deleted($model)
    {
        $categorias = Categoria::select('titulo', 'slug')->ordered()->get();
        Cache::put('categorias', $categorias, 0);
    }
}
