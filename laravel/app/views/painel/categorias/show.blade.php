@extends('painel._layouts.default')

@section('main')

<h1>Categoria</h1>

<p>{{ link_to_route('painel.categorias.index', 'Listar categorias', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
                <th>Descricao</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $categoria->titulo }}}</td>
                    <td>{{ $categoria->descricao }}</td>
                    <td>
                        @if ( $categoria->imagem )
                            <img width="100" src="{{ URL::asset($categoria->imagem) }}" alt="{{ $categoria->titulo }}">
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($categoria->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.categorias.edit', 'Editar', array($categoria->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.categorias.destroy', $categoria->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
