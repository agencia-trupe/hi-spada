<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TubainFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return new \Tubain;
    }

}
