<?php namespace App\Controllers\Painel;

use App\Controllers\Painel\RelatedUploadController;
use MidiaFoto;

class MidiaFotosController extends RelatedUploadController
{

    /**
     * MidiaFotos constructor
     *
     * @param ProjetoFoto $midiaFoto Instância do repositório MidiaFoto
     */
    public function __construct(Midiafoto $midiaFoto)
    {
        parent::__construct($midiaFoto);
    }

}
