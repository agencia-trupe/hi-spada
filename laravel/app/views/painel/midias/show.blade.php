@extends('painel._layouts.default')

@section('main')

<h1>Publicação</h1>

<p>{{ link_to_route('painel.midias.index', 'Listar publicações', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
                <th>Descricao</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $midia->titulo }}}</td>
                    <td>{{ $midia->descricao }}</td>
                    <td>
                        @if ( $midia->imagem )
                            <img width="100" src="{{ URL::asset($midia->imagem) }}" alt="{{ $midia->titulo }}">
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($midia->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.midias.edit', 'Editar', array($midia->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.midias.destroy', $midia->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
