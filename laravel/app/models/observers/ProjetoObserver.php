<?php

class ProjetoObserver
{
    public function saved($model)
    {
        if ($model->destaque) {
            $projetosDestacados = Projeto::where('destaque', true)->ordered()->with('categoria')->get();
            Cache::put('projetosDestacados', $projetosDestacados, 0);
        }
    }

    public function deleted($model)
    {
        if ($model->destaque) {
            $projetosDestacados = Projeto::where('destaque', true)->ordered()->with('categoria')->get();
            Cache::put('projetosDestacados', $projetosDestacados, 0);
        }
    }
}
