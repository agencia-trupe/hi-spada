<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Categoria, Image, Profissional, Response;

class ProfissionalsController extends BaseController
{
    /**
     * Categoria Repository
     *
     * @var Categoria
     */
    protected $profissional;

    public function __construct(Profissional $profissional)
    {
        $this->profissional = $profissional;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $profissionals = $this->profissional->all();

        return View::make('painel.profissionals.index', compact('profissionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //Array com categorias disponíveis e seus respectivos ids como chave.
        return View::make('painel.profissionals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), array('_method'));;
        $validation = Validator::make($input, Profissional::$rules);

        if ($validation->passes()) {

            Profissional::create($input);

            if (Input::ajax()) {
                return json_encode(
                    array(
                        'errors' => false,
                        'message' => 'Cadastro efetuado com sucesso'
                        )
                    );
            }

            return Redirect::route('painel.profissionais.index');
        }

        if (Input::ajax()) {
            return json_encode(
                    array(
                        'errors' => $validation->errors()->toArray(),
                        )
                    );
        }

        return Redirect::route('painel.profissionais.create')
            ->withInput()
            ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $profissional = $this->profissional->findOrFail($id);

        return View::make('painel.profissionals.show', compact('profissional'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $profissional = $this->profissional->find($id);

        if (is_null($profissional)) {
            return Redirect::route('painel.profissionais.index');
        }

        return View::make('painel.profissionals.edit', compact('profissional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {

        $input = array_except(Input::all(), array('_method'));

        $validation = Validator::make($input, Profissional::$rules);

        if ($validation->passes()) {
            $profissional = $this->profissional->find($id);

            $profissional->update($input);

            return Redirect::route('painel.profissionais.show', $id);
        }

        return Redirect::route('painel.profissionais.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $profissional = $this->profissional->find($id);

        $profissional->delete();

        return Redirect::route('painel.profissionais.index');
    }

}
