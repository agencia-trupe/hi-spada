<?php

/**
 * @todo Constructor
 */

class Tubain
{

    public function getVideoFeed( $videoUrl )
    {
        $videoId = $this->getVideoId( $videoUrl );

        return file_get_contents("http://gdata.youtube.com/feeds/api/videos/"
                                    . $videoId . "?v=2&alt=jsonc");
    }

    public function getVideoId( $videoUrl )
    {
        if (strpos($videoUrl, 'http') !== 0) {
            throw new Exception("Invalid Video Url", 1);
        }

        parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $videoUrlVars );

        return $videoUrlVars['v'];
    }

    public function getThumbnailUrl( $videoUrl, $quality = NULL)
    {
        $videoId = $this->getVideoId($videoUrl);
        $jsonFeed = $this->getVideoFeed( $videoUrl );
        $decodedFeed = json_decode($jsonFeed);

        return $decodedFeed->data->thumbnail->sqDefault;
    }

    public function parseThumbnail( $videoUrl )
    {
        $thumbnailUrl = $this->getThumbnailUrl( $videoUrl );

        return '<img src="' . $thumbnailUrl . '" alt="" />';
    }

    public function getVideoThumbnails( $videoUrl )
    {
        $decodedFeed = json_decode($this->getVideoFeed( $videoUrl ));
        foreach ($decodedFeed->data->thumbnail as $key => $value) {
            $videoThumbnails[$key] = $value;
        }

        return $videoThumbnails;
    }

    public function saveLocalThumbnails($videoUrl, $localPath)
    {
        $videoThumbnails = $this->getVideoThumbnails( $videoUrl );
        $videoId = $this->getVideoId( $videoUrl );
        $fileDir = $localPath . $videoId;

        if ( ! is_dir($fileDir) ) {
            // dir doesn't exist, make it
            $old = umask(0);
            mkdir($fileDir, 0777);
            umask($old);
        }

        foreach ($videoThumbnails as $key => $value) {
            $filename = $fileDir . '/' . $key . '.jpg';

            if( ! file_put_contents( $filename, file_get_contents( $value ) ) )
                throw new Exception("Erro ao salvar thumbnail " . $value , 1);

            $result[] = $filename;
        }

        return $result;
    }

    public function buildEmbed( $videoId = null, $videoUrl = null, $width, $height)
    {
        if (! $videoId) {
            $videoId = $this->getVideoId( $videoUrl );
        }

        return  '<iframe width="' . $width . '" height="' . $height .
                '" src="//www.youtube.com/embed/' . $videoId .
                '" frameborder="0" allowfullscreen></iframe>';
    }
}
