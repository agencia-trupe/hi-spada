<?php namespace App\Composers;

/**
 * SocialsComposer.php
 *
 * SocialsComposer
 *
 * Compartilha os dados de redes sociais com a view de layout.
 *
 * @package     Arycom
 * @category    Composers
 * @framework   Laravel
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use Cache, Social;

class SocialsComposer
{
    public function compose($view)
    {
    	$socials = Social::orderBy('titulo')->get();
        $view->with('socials', $socials);
    }

}
