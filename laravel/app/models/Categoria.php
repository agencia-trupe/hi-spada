<?php

class Categoria extends Base
{
    protected $guarded = array();

    public static $rules = array(
            'titulo' => 'required',
        );

    public function projetos()
    {
        return $this->hasMany('Projeto');
    }
}
