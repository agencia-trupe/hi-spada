<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Slide, Image, File, Parser, Templates;

class SlidesController extends BaseController
{
    /**
     * Slide Repository
     *
     * @var Slide
     */
    protected $slide;

    public function __construct(Slide $slide)
    {
        $this->slide = $slide;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $slides = $this->slide->ordered()->get();

        return View::make('painel.slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templatesNames = Templates::getTemplates();

        return View::make('painel.slides.create')->with('templates', $templatesNames);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Slide::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'slides');
            }
            $this->slide->create($input);

            return Redirect::route('painel.slides.index');
        }

        return Redirect::route('painel.slides.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $slide = $this->slide->findOrFail($id);

        return View::make('painel.slides.show', compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $slide = $this->slide->find($id);

        if (is_null($slide)) {
            return Redirect::route('painel.slides.index');
        }

        return View::make('painel.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Slide::$rules);

        if ($validation->passes()) {
            $slide = $this->slide->find($id);

            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'slides');
                @unlink(public_path(substr($slide->imagem, 1)));
            }

            $slide->update($input);

            return Redirect::route('painel.slides.show', $id);
        }

        return Redirect::route('painel.slides.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $slide = $this->slide->find($id);

        @unlink(public_path(substr($slide->imagem, 1)));

        $slide->delete();

        return Redirect::route('painel.slides.index');
    }

}
