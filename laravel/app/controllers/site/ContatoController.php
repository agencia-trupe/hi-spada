<?php namespace App\Controllers\Site;

use View;
use Local;
use App\Services\Validators\ContatoValidator;
use App\Services\Validators\CurriculoValidator;
use Mail;
use Input;
use Image;

class ContatoController extends \BaseController
{
    public function index()
    {
        $contato = Local::first();

        return View::make('site.contato.index', compact('contato'));
    }

    public function sendMail()
    {
        $validator = new ContatoValidator;

        if ($validator->passes()) {
            $mail = Mail::send('site.contato.email-contato', Input::all(), function($message) {
                $message->to('hi-spada@hi-spada.com.br', 'Contato - Hi-Spada')
                        ->subject('Contato do Site Hi-Spada - ' . Input::get('nome'))
						->bcc('noreply@hi-spada.com.br');
											
            });

            echo json_encode(
                array('errors' => false,
                      'msg' => 'Mensagem enviada com sucesso!'
                    ));
        } else {
            $errors = null;
            foreach ($validator->getErrors()->all() as $error) {
                $errors .= $error . "\n";
            }
            echo json_encode(
                array( 'errors' => $errors,
                       'msg' => 'Corrija os seguintes erros de validação:'));
        }
    }

    public function curriculos()
    {
        if ( !Input::hasFile('curriculo') ) {
            return json_encode(
                array( 'errors' => 'Você precisa selecionar um arquivo',
                       'msg' => 'Corrija os seguintes erros de validação:'));
        }

        $validator = new CurriculoValidator;

        $cv = Input::file('curriculo');
        $filename = $cv->getClientOriginalName();
        $cv->move('public_html/uploads/curriculos', $filename);
        $fullpath = 'public_html/uploads/curriculos/'.$filename;

        if ($validator->passes()) {

            $mail = Mail::send('site.contato.email-trabalhe', Input::all(), function($message) use ($fullpath) {
                $message->to('noreply@hi-spada.com.br', 'Contato - Hi-Spada')
                        ->subject('Contato do Hi-Spada - ' . Input::get('nome'))
                        ->bcc('spadamoveis@ig.com.br')
                        ->attach($fullpath);
            });

            echo json_encode(
                array('errors' => false,
                      'msg' => 'Mensagem enviada com sucesso!'
                    ));
        } else {
            $errors = null;
            foreach ($validator->getErrors()->all() as $error) {
                $errors .= $error . "\n";
            }
            echo json_encode(
                array( 'errors' => $errors,
                       'msg' => 'Corrija os seguintes erros de validação:'));
        }
    }
}
