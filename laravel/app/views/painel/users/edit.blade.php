@extends('painel._layouts.default')

@section('main')

<h1>Editar Rede Social</h1>
{{ Form::model($user, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('painel.users.update', $user->id))) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('link') ? 'error' : '' }}">
        {{ Form::label('link', 'Link:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('link', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('link') }}<h5>
    </div>
        <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 16px; height: 16px;">
            @if ($user->imagem)
                <a href="<?php echo $user->imagem; ?>"><img src="<?php echo asset($user->imagem); ?>" alt=""></a>
            @else
                <img src="http://www.placehold.it/16x16/EFEFEF/AAAAAA">
            @endif
        </div>
        <div>
            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.users.show', 'Cancelar', $user->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
