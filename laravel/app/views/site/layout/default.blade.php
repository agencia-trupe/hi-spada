<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Hi-Spada</title>
        <meta name="description" content="Hi-Spada móveis planejados, fabricação, montagem, pós-venda e assistência técnica.">
        <meta name="keywords" content="moveis planejados, quartos planejados, cozinhas planejadas, salas planejadas, mobiliário planejado, armários planejados.">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css')}}">
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.6.2.min.js')}}"></script>

    </head>
    <body <?php echo (isset($pagina)) ? 'class="' . $pagina . '"' : '' ?>>
        <header>
            <div class="interna">
                <div class="clearfix"></div>
                <a href="{{ URL::to('/') }}" class="marca">
                    Hi Spada - Movelaira e Design
                </a>
                <div class="sociais">
                    @foreach ($socials as $social)
                        @if($social->link != '')
                            <a href="{{$social->link}}" class="social {{ strtolower($social->titulo) }}" title="{{ strtolower($social->titulo) }}"></a>
                        @endif
                    @endforeach
                </div>
                <nav>
                        <ul>
                            <li>
                                <a class="{{ Request::is('empresa') ? 'active' : '' }} nav-item-empresa" href="{{ URL::to('empresa')}}">
                                    <span class="icone"></span>
                                    <span class="titulo">Empresa</span>
                                </a></li>
                            <li class="projetos-nav">
                                <a class="{{ Request::is('projetos*') ? 'active' : '' }} nav-item-projetos" href="{{ URL::to('projetos')}}">
                                    <span class="icone"></span>
                                    <span class="titulo">Projetos</span></a>
                                    <span class="nav-categorias">
                                        <ul>
                                            @foreach ($navCategorias as $navCategoria)
                                                <li><a href="{{ URL::route('projetos', $navCategoria->slug) }}">{{ strtolower($navCategoria->titulo) }}</a></li>
                                            @endforeach
                                        </ul>
                                    </span>
                            </li>
                            <li>
                                <a class="{{ Request::is('publicacoes') ? 'active' : '' }} nav-item-publicacoes" href="{{ URL::to('publicacoes')}}">
                                    <span class="icone"></span>
                                    <span class="titulo">Publicações</span></a></li>
                            <li>
                                <a class="{{ Request::is('profissionais') ? 'active' : '' }} nav-item-profissionais" href="{{ URL::to('profissionais')}}">
                                    <span class="icone"></span>
                                    <span class="titulo">Profissionais</span></a></li>
                            <li>
                                <a class="{{ Request::is('contato') ? 'active' : '' }} nav-item-contato" href="{{ URL::to('contato')}}">
                                    <span class="icone"></span>
                                    <span class="titulo">Contato</span></a></li>
                        </ul>
                    </nav>
                <div class="clearfix"></div>
            </div>
        </header>
        @yield('main')

        <footer>
            <div class="interna">
                <nav class="footer-paginas">
                    <ul>
                        <li class="{{ Request::is('empresa') ? 'active' : '' }}">
                            <a class="nav-item-empresa" href="{{ URL::to('empresa')}}">
                                <span class="icone"></span>
                                <span class="titulo">Empresa</span>
                            </a></li>
                        <li class="{{ Request::is('projetos') ? 'active' : '' }}">
                            <a class="nav-item-projetos" href="{{ URL::to('projetos')}}">
                                <span class="icone"></span>
                                <span class="titulo">Projetos</span></a></li>
                        <li class="{{ Request::is('publicacoes') ? 'active' : '' }}">
                            <a class="nav-item-publicacoes" href="{{ URL::to('publicacoes')}}">
                                <span class="icone"></span>
                                <span class="titulo">Publicações</span></a></li>
                        <li class="{{ Request::is('profissionais') ? 'active' : '' }}">
                            <a class="nav-item-profissionais" href="{{ URL::to('profissionais')}}">
                                <span class="icone"></span>
                                <span class="titulo">Profissionais</span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </nav>
                <nav>
                    <ul>
                        @foreach ($navCategorias as $navCategoria)
                            <li><a href="{{ URL::to('/projetos/' . $navCategoria->slug) }}">{{ strtolower($navCategoria->titulo) }}</a></li>
                        @endforeach
                    </ul>
                </nav>
                <div class="separador-vertical-footer"></div>
                <div class="social-footer-container">
                    <h3>Siga</h3>
                    <div class="clearfix"></div>
                    @foreach ($socials as $social)
                        <a href="" class="social {{ strtolower($social->titulo) }}">
                        </a>
                    @endforeach
                </div>
                <div class="local-footer">
                    <div class="contato-footer">
                        <h3>Contato</h3>
                        <div class="contato-footer-telefone">
                            <span class="icone"></span>  <span>{{ $contato->ddd1 }} {{ $contato->telefone1 }}</span>
                        </div>
                        <div class="clearfix"></div>
                        <a href="mailto://{{ $contato->email }}" class="contato-footer-email">{{ $contato->email }}</a>
                    </div>
                    <div class="endereco-footer">
                        <h3>Endereço</h3>
                        <p>{{ $contato->endereco }} {{ $contato->numero }}
                            @if ($contato->complemento) {{ $contato->complemento}}
                            @endif
                             {{ $contato->bairro }} <br>
                            {{ $contato->cidade }}, {{ $contato->uf }} - CEP: {{ $contato->cep }}
                        </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ URL::asset('js/vendor/jquery-1.9.1.min.js') }}"><\/script>')</script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="{{ URL::asset( 'assets/js/plugins.js' ) }}"></script>
        <script src="{{ URL::asset( 'assets/js/main.js' ) }}"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-55410832-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
