<?php

class Midia extends Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' 	=> 	'required|max:255',
    );

    public function midiaFotos()
    {
        return $this->hasMany('Midiafoto')->ordered();
    }
}
