<?php

class Parser
{
	
	/**
	* Retrieve metadata from a file.
	*
	* Searches for metadata in the first 8kiB of a file, such as a plugin or theme.
	* Each piece of metadata must be on its own line. Fields can not span multiple
	* lines, the value will get cut at the end of the first line.
	*
	* If the file data is not within that first 8kiB, then the author should correct
	* their plugin file and move the data headers to the top.
	*
	* @see http://codex.wordpress.org/File_Header
	*
	* @since 2.9.0
	* @param string $file Path to the file
	*/
	public static function getFileData ( $file, $default_headers )
	{
	    // We don't need to write to the file, so just open for reading.
	    $fp = fopen( $file, 'r' );
	
	    // Pull only the first 8kiB of the file in.
	    $file_data = fread( $fp, 8192 );
	
	    // PHP will close file handle, but we are good citizens.
	    fclose( $fp );
	
	    // Make sure we catch CR-only line endings.
	    $file_data = str_replace( "\r", "\n", $file_data );
	
	    foreach ( $default_headers as $field => $regex ) {
	            if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] )
	                    $default_headers[ $field ] = self::_cleanup_header_comment( $match[1] );
	            else
	                    $default_headers[ $field ] = '';
	    }
	
	    return $default_headers;
	}

	/**
	* Strip close comment and close php tags from file headers used by WP.
	* See http://core.trac.wordpress.org/ticket/8497
	*
	* @since 2.8.0
	*
	* @param string $str
	* @return string
	*/
	private static function _cleanup_header_comment($str) {
	        return trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $str));
	}
}