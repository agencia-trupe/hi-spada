<?php namespace App\Controllers\Painel;

    use Auth, BaseController, Form, Input, Redirect, Sentry, View;

    class AuthController extends BaseController
    {
        public function getLogin()
        {
            return View::make('painel.auth.login');
        }

        public function postLogin()
        {
            $credentials = array(
                'username'    => Input::get('username'),
                'password' => Input::get('password')
            );

            try {
                $user = Sentry::authenticate($credentials, false);

                if ($user) {
                    return Redirect::route('painel.paginas.index');
                }
            } catch (\Exception $e) {
                return Redirect::route('painel.login')->withErrors(array('login' => $e->getMessage()));
            }
        }

        public function getLogout()
        {
            Sentry::logout();

            return Redirect::route('painel.login');
        }

    }
