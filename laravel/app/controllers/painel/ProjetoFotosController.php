<?php namespace App\Controllers\Painel;

use App\Controllers\Painel\RelatedUploadController;
use Projetofoto;

class ProjetoFotosController extends RelatedUploadController
{

    /**
     * ProjetoFotos constructor
     *
     * @param ProjetoFoto $projetoFoto Instância do repositório ProjetoFoto
     */
    public function __construct(Projetofoto $projetoFoto)
    {
        parent::__construct($projetoFoto);
    }

}
