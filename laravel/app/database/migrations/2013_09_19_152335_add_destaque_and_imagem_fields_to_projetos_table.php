<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDestaqueAndImagemFieldsToProjetosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projetos', function(Blueprint $table) {
			$table->boolean('destaque');
			$table->string('imagem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projetos', function(Blueprint $table) {
			$table->dropColumn('destaque');
			$table->dropColumn('imagem');
		});
	}

}
