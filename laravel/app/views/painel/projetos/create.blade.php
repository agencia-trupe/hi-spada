@extends('painel._layouts.default')

@section('main')

<h1>Criar Projeto</h1>

<div class="row-fluid">
    <div class="span7">
        {{ Form::open(array('route' => 'painel.projetos.store', 'files' => TRUE, 'data-model-name' => 'projeto', 'class' => 'related', 'data-saved' => 'false', 'data-send-url' => '/painel/projetos')) }}
                {{ Form::hidden('object_id') }}
                <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
                    {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('titulo', NULL, array('class' => 'span10')) }}
                    </div>
                    <h5>{{ $errors->first('titulo') }}<h5>
                </div>
                <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
                    {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::text('slug', NULL, array('class' => 'span10', 'readonly')) }}
                    </div>
                    <h5>{{ $errors->first('slug') }}<h5>
                </div>
                <div class="control-group {{ $errors->first('categoria_id') ? 'error' : '' }}">
                    {{ Form::label('categoria_id', 'Categoria:', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('categoria_id', $categorias, array('class' => 'span7', 'readonly')) }}
                    </div>
                    <h5>{{ $errors->first('categoria_id') }}<h5>
                </div>
                <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
                    {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::textarea('descricao', NULL, array('class' => 'span6 ceditor', 'rows' => 7)) }}
                    </div>
                    <h5>{{ $errors->first('descricao') }}<h5>
                </div>
                <div class="control-group">
                    {{ Form::label('imagem', 'Imagem em destaque') }}

                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
                        </div>
                        <div>
                            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
                        </div>
                    </div>
                </div>
                <div class="control-group {{ $errors->first('destaque') ? 'error' : '' }}">
                    {{ Form::label('destaque', 'Projeto em destaque:', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::checkbox('destaque', 1) }}
                    </div>
                    <h5>{{ $errors->first('destaque') }}<h5>
                </div>

                    {{ Form::submit('Salvar', array('class' => 'btn projeto_sumbit')) }}
        {{ Form::close() }}
    </div>
    <div class="span5 well">
        <h2>Fotos do projeto</h2>
        <h6 class="text-info">Você poderá adicionar fotos após salvar o projeto.</h6>
    </div>
</div>
@stop
