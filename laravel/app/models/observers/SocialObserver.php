<?php

class SocialObserver
{
    public function saved($model)
    {
        $sociais = Social::select('titulo', 'link', 'imagem')->ordered()->get();
        Cache::put('sociais', $sociais, 0);
    }

    public function deleted($model)
    {
        $sociais = Social::select('titulo', 'link', 'imagem')->ordered()->get();
        Cache::put('sociais', $sociais, 0);
    }
}
