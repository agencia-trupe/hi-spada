<?php

class SlidesObserver
{
    public function saved($model)
    {
        $slides = Slide::select('titulo', 'link', 'imagem')->ordered()->get();
        Cache::put('slides', $slides, 0);
    }

    public function deleted($model)
    {
        $slides = Slide::select('titulo', 'link', 'imagem')->ordered()->get();
        Cache::put('slides', $slides, 0);
    }
}
